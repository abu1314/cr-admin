import axios from "axios";
import { Message } from 'element-ui';
import router from '@/router/index.js';

import NProgress from 'nprogress'
import 'nprogress/nprogress.css'


const instance = axios.create({
    //baseURL将会自动加到get,post,put,delete请求路径前
    baseURL: 'http://localhost/api',
    // 超时时间
    timeout: 10000
});

// 全局请求拦截：所有的网络请求先走这个
instance.interceptors.request.use(config => {

    NProgress.start();
    // 将token放在headers中的 token字段中
    const token = sessionStorage.getItem("CR-TOKEN");
    token && (config.headers.token = token);
    return config;
}, function(error) {
    return Promise.reject(error);
});

// 全局响应拦截，可以根据响应不同的状态码跳转到不同的页面
instance.interceptors.response.use(resp => {
    NProgress.done();
    // 后置拦截器输出data
    let data = resp.data

    if (data.code === 1) {
        return resp;
    } else if (data.code === 401) {
        Message.warning(data.msg)
        router.push('login');
    } else if (data.code === 420) {
        Message.warning(data.msg)
        router.push('login');
    } else if (data.code === 421) {

    } else if (data.code === 204) {
        // 没有为角色赋予任何权限信息只进行提示
        Message.info(data.msg)
    } else {
        Message.info(data.msg)
        Promise.reject(res.msg);
    }


}, error => {
    //接收到异常进行处理 
    console.log(error);
    return Promise.reject(error);
});

// 导出axios实例  instance
export default instance;