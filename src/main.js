// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
// 引入路由
import router from './router'
Vue.config.productionTip = false


// 引入element-ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
// 引入全局样式表
import '@/assets/css/global.css';

// 引入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'
// require styles 引入样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
// 引入echarts
import echarts from 'echarts'
Vue.prototype.$echarts = echarts


Vue.use(VueQuillEditor)

// 引入vuex
import store from '@/store/store';

import axiosinstance from '@/utils/request';
Vue.prototype.$http = axiosinstance


/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
})