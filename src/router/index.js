import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/manager/Login'
import ManagerList from '@/components/manager/ManagerList';
import Welcome from '@/components/Welcome';
import Page404 from '@/components/error/404';
import unauthorize from '@/components/error/403';
import Classroom from '@/components/classroom/Classroom';
import Cla from '@/components/classroom/Cla';
import Course from '@/components/classroom/Course';
import Permission from '@/components/rights/Permission';
import Role from '@/components/rights/Role';
import Order from '@/components/order/Order';
import Notice from '@/components/notice/Notice';

import { Message } from 'element-ui';
Vue.use(Router)

const router = new Router({
    routes: [{
            path: '/',
            redirect: "/home"
        }, {
            path: '/home',
            component: Home,
            redirect: '/welcome',
            children: [{
                path: '/welcome',
                component: Welcome
            }, {
                path: '/managerlist',
                component: ManagerList,
                meta: {
                    permission: true,
                    permissionId: '207'
                }
            }, {
                path: '/classroomlist',
                component: Classroom,
                meta: {
                    permission: true,
                    permissionId: '206'
                }
            }, {
                path: '/course',
                component: Course,
                meta: {
                    permission: true,
                    permissionId: '211'
                }
            }, {
                path: '/permission',
                component: Permission,
                meta: {
                    permission: true,
                    permissionId: '210'
                }
            }, {
                path: '/role',
                component: Role,
                meta: {
                    permission: true,
                    permissionId: '209'
                }
            }, {
                path: '/cla',
                component: Cla,
                meta: {
                    permission: true,
                    permissionId: '212'
                }
            }, {
                path: '/order',
                component: Order,
                meta: {
                    permission: true,
                    permissionId: '208'
                }
            }, {
                path: '/Notice',
                component: Notice,
                meta: {
                    permission: true,
                    permissionId: '213'
                }
            }, {
                path: '/unauthorize',
                component: unauthorize
            }]
        }, {
            path: '/login',
            component: Login
        }, {
            path: '*',
            component: Page404
        }

    ]

})



// 全局守卫，控制访问不同的页面需要的权限验证等信息
router.beforeEach((to, from, next) => {
    // 获取tokeen
    const token = sessionStorage.getItem("CR-TOKEN");
    // 获取权限列表
    // const pIdList = ['207', '209'];
    const pIdListStr = sessionStorage.getItem('pIdList')
        // console.log(pIdListStr);

    const pIdList = JSON.parse(pIdListStr);

    let permissionId = to.meta.permissionId
    if (to.path != '/login' && !token) {
        // 提示，引导到登录界面中
        Message.warning('请先登录')
        next('/login');
    } else if (to.meta.permission) {
        // 判断该角色是否有相应的访问权限
        for (var i = 0; i < pIdList.length; i++) {
            if (pIdList[i] === permissionId) {
                next()
                break
            } else if (i === pIdList.length - 1) {
                next({
                    path: '/unauthorize'
                })
                Message.error('您没有权限访问呢')

            }
        }
    } else {
        // 放行
        next()
    }
})

export default router;