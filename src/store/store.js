import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
        nonInspectCount: 0 //存储了一个公共状态（数据）
    },
    mutations: { //修改共享状态，方法的第一个参数永远是state
        setCount(state, nonInspectCount) {
            state.nonInspectCount = nonInspectCount;
        }
    },
    getters: {
        getCount: state => {
            return state.nonInspectCount
        }
    }
})
export default store